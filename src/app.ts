
import {default as xs, Stream, MemoryStream} from 'xstream'
import debounce from 'xstream/extra/debounce'
import dropUntil from 'xstream/extra/dropUntil'
import {ul, li, span, input, div, section, button, svg, VNode} from '@cycle/dom'
import {
  Sources, Sinks, MainDOMSource, TimeSource,
  SuggestionsConfigType, AutocompleteStateType, AutocompleteReducerType, 
  ReducerInputType, ActionsType, JSONPSource, CycleMouseEvent
} from './types'
const {path, g} = svg

const macroContainerStyle = {
  width: '100%',
  'font-family': 'system-ui, sans-serif'
}
const containerStyle = {
  width: '100%',
  'max-width': '20em',
  margin: 'auto'
}

const sectionStyle = {
  marginBottom: '.625em',
}

const searchLabelStyle = {
  display: 'inline-block',
  'margin-right': '.5em'
}

const comboBoxStyle = {
  position: 'relative',
  display: 'inline-block',
  width: '100%'
}

const inputTextStyle = {
  padding: '.25em',
}

const autocompleteableStyle = {
  ...inputTextStyle, 
  width: '100%',
  boxSizing: 'border-box',
  padding: '.25em',
  'font-size': '1em'
}

const autocompleteMenuStyle = {
  position: 'absolute',
  left: '0',
  right: '0',
  top: '2em',
  zIndex: '999',
  listStyle: 'none',
  backgroundColor: 'white',
  margin: '0',
  padding: '0',
  borderTop: '1px solid #ccc',
  borderLeft: '1px solid #ccc',
  borderRight: '1px solid #ccc',
  boxSizing: 'border-box',
  boxShadow: '0 .25em .25em rgb(220,220,220)',
  userSelect: 'none',
  '-moz-box-sizing': 'border-box',
  '-webkit-box-sizing': 'border-box',
  '-webkit-user-select': 'none',
  '-moz-user-select': 'none',
}

const autocompleteItemStyle = {
  cursor: 'pointer',
  listStyle: 'none',
  padding: '.5em',
  margin: '0',
  borderBottom: '1px solid #ccc',
}

const selectedListStyle = {
  '-moz-box-sizing': 'border-box',
  '-webkit-box-sizing': 'border-box',
  'list-style': 'none',
  'width': '100%',
  'margin-bottom': '0',
  'padding-inline-start': '0',
  'margin-top': '0'
}

const selectedItemStyle = {
  display: 'flex',
  'align-items': 'center',
  'justify-content': 'space-between',
  boxSizing: 'border-box',
  'margin-top': '.5em',
  'margin-bottom': '.25em',
  'padding': '.5em 0 .5em .5em',
  background: 'lightslategray',
  color: 'white',
  'border-radius': '.125em',
}

const deleteXButtonStyle = {
  display: 'flex',
  'align-items': 'center',
  color: 'white',
  background: 'transparent',
  border: 'none',
  'text-decoration': 'none',
  'box-shadow': 'none'
}

const noResultsListStyle = {
  color: 'gray',
  'font-weight': 'normal',
  width: '100%',
  'list-style': 'none',
  'padding-inline-start': '0'
}

const noResultsItemStyle = {
  width: '100%'
}

const noResultsStyle = {
  display: 'inline-block',
  color: 'gray',
  margin: 'auto'
}

const LIGHT_GRAY = 'lightgray'

export function renderCloseIcon(hw: number = 18) {
  return svg({
    "attrs": {
      "viewBox": "0 0 9 9",
      "version": "1.1",
      "xmlns": "http://www.w3.org/2000/svg",
      "xlink": "http://www.w3.org/1999/xlink",
      "height": `${hw}px`,
      "width": `${hw}px`,
    }
  }, [
    g("#Page-1", {
      "attrs": {
        "stroke": "none",
        "stroke-width": "1",
        "fill": "none",
        "fill-rule": "evenodd",
        "stroke-linecap": "round",
        "stroke-linejoin": "round"
      },
      "id": {
        "name": "id",
        "value": "Page-1"
      }
    }, [
      g("#iPhone-SE", {
        "attrs": {
          "transform": "translate(-107.000000, -77.000000)",
          "stroke": "#FFFFFF",
          "stroke-width": "1"
        },
        "id": {
          "name": "id",
          "value": "iPhone-SE"
        }
      }, [
        g("#close_icon", {
          "attrs": {
            "transform": "translate(108.000000, 78.000000)"
          },
          "id": {
            "name": "id",
            "value": "close_icon"
          }
        }, [
          path("#Path-11", {
            "attrs": {
              "d": "M0,0.500001907 L6.36396103,6.86396294"
            },
            "id": {
              "name": "id",
              "value": "Path-11"
            }
          }),
          path("#Path-11", {
            "attrs": {
              "d": "M0,6.86396294 L6.36396103,0.500001907"
            },
            "id": {
              "name": "id",
              "value": "Path-11"
            }
          })
        ])
      ])
    ])
  ])
  // let {g, path, svg} = hh(h);
}

export function getStateStream(sources: Sources): MemoryStream<AutocompleteStateType> { 
  return sources.state.stream
}

/**
 * source: --a--b----c----d---e-f--g----h---i--j-----
 * first:  -------F------------------F---------------
 * second: -----------------S-----------------S------
 *                         between
 * output: ----------c----d-------------h---i--------
 */
function between(first: Stream<any>, second: Stream<any>) {
  return (source: Stream<any>) => first.mapTo(source.endWhen(second)).flatten()
} 

/**
 * source: --a--b----c----d---e-f--g----h---i--j-----
 * first:  -------F------------------F---------------
 * second: -----------------S-----------------S------
 *                       notBetween
 * output: --a--b-------------e-f--g-----------j-----
 */
function notBetween(first: Stream<any>, second: Stream<any>) {
  return (source: Stream<any>) => xs.merge(
    source.endWhen(first),
    first.map(() => source.compose(dropUntil(second))).flatten()
  )
}

function intent(domSource: MainDOMSource, timeSource: TimeSource): ActionsType {
  const UP_KEYCODE = 38
  const DOWN_KEYCODE = 40
  const ENTER_KEYCODE = 13
  const TAB_KEYCODE = 9

  const input$ = domSource.select('.autocompleteable').events('input')
  const keydown$ = domSource.select('.autocompleteable').events('keydown')
  const itemHover$ = domSource.select('.autocomplete-item').events('mouseenter')
  const itemMouseDown$ = domSource.select('.autocomplete-item').events('mousedown')
  const itemMouseUp$ = domSource.select('.autocomplete-item').events('mouseup')
  const inputFocus$ = domSource.select('.autocompleteable').events('focus')
  const inputBlur$ = domSource.select('.autocompleteable').events('blur')

  const removeItem$ = domSource.select('button').events('click')
    .map(ev => parseInt(((ev as CycleMouseEvent).ownerTarget).dataset.index as string))

  const enterPressed$ = keydown$.filter(({keyCode}) => keyCode === ENTER_KEYCODE)
  const tabPressed$ = keydown$.filter(({keyCode}) => keyCode === TAB_KEYCODE)
  const clearField$ = input$.filter(ev => (ev.target as HTMLInputElement).value.length === 0)
  const inputBlurToItem$ = inputBlur$.compose(between(itemMouseDown$, itemMouseUp$))
  const inputBlurToElsewhere$ = inputBlur$.compose(notBetween(itemMouseDown$, itemMouseUp$))
  const itemMouseClick$ = itemMouseDown$
    .map(down => itemMouseUp$.filter(up => down.target === up.target))
    .flatten() 

  return {
    search$: input$
      .compose(timeSource.debounce(500))
      .compose(between(inputFocus$, inputBlur$))
      .map(ev => ev.target.value)
      .filter(query => query.length > 0),
    moveHighlight$: keydown$
      .map<number>(({keyCode}) => { switch (keyCode) {
        case UP_KEYCODE: return -1
        case DOWN_KEYCODE: return 1
        default: return 0
      }})
      .filter(delta => delta !== 0),
    setHighlight$: itemHover$
      .map(ev => parseInt((ev.target as HTMLElement).dataset.index as string)),
    keepFocusOnInput$:
      xs.merge(inputBlurToItem$, enterPressed$, tabPressed$),
    selectHighlighted$:
      xs.merge(itemMouseClick$, enterPressed$, tabPressed$).compose(debounce(1)),
    wantsSuggestions$:
      xs.merge(inputFocus$.mapTo(true), inputBlur$.mapTo(false)),
    quitAutocomplete$:
      xs.merge(clearField$, inputBlurToElsewhere$),
    removeItem$ 
  }
} 

function getFilteredSuggestions(suggestions: string[], listed: string[]) {
  return suggestions
    .filter(suggestion => !listed.some(sel => sel == suggestion))
}

function reducers(suggestionsFromResponse$: Stream<string[]>, actions: ActionsType): Stream<AutocompleteReducerType> {
  const moveHighlightReducer$ = actions.moveHighlight$
    .map(delta => function moveHighlightReducer(state: AutocompleteStateType)  {
      const suggestions = state.suggestions
      const wrapAround = (x: number) => (x + suggestions.length) % suggestions.length
      const highlighted = state.highlighted
      return {
        ...state, 
        highlighted: highlighted === null ? 
          wrapAround(Math.min(delta, 0)) : 
          wrapAround(highlighted + delta)
      }
    })

  const setHighlightReducer$ = actions.setHighlight$
    .map(highlighted => function setHighlightReducer(state: AutocompleteStateType) {
      return { ...state, highlighted }
    })

  const selectHighlightedReducer$ = actions.selectHighlighted$
    .mapTo(xs.of(true, false))
    .flatten()
    .map(selected => function selectHighlightedReducer(state: AutocompleteStateType) {
      const suggestions = state.suggestions
      const highlighted = state.highlighted
      const hasHighlight = highlighted !== null
      const isMenuEmpty = suggestions.length === 0

      if (selected && hasHighlight && !isMenuEmpty) {
        return {
          ...state,
          listed: state.listed.concat([suggestions[highlighted as number]]),
          suggestions: [],
          cachedSuggestions: [],
          highlighted: null,
          selected: null//selected // ? null : false
        }
      } else {
        return {...state, selected: false}
      }
    })

  const hideReducer$ = actions.quitAutocomplete$
    .mapTo(function hideReducer(state: AutocompleteStateType) {
      return { ...state, suggestions: [], highlighted: null, selected: false}
    })

  const suggestions$ = actions.wantsSuggestions$
    .filter(Boolean)
    .map(accepted => {
      return xs.merge<SuggestionsConfigType>(
        suggestionsFromResponse$.map(suggestions => accepted ? suggestions : [])
          .map<SuggestionsConfigType>(results => ({type: 'direct', results})),
        xs.of<SuggestionsConfigType>({type: 'cached'})
      )
    })
    .flatten()
    .map(({type, results}) => function suggestionsReducer(state: AutocompleteStateType) {
      if (type === "direct") {
        const suggestions = getFilteredSuggestions(results as string[], state.listed)
        return {...state, suggestions, cachedSuggestions: suggestions, highlighted: null, selected: false}
      } else {
        const suggestions = getFilteredSuggestions((state.cachedSuggestions || []), state.listed)
        return {...state, suggestions, cachedSuggestions: suggestions}
      }
    })

  const removeItemReducer$ = actions.removeItem$
    .map(index => function removeItemReducer(state: AutocompleteStateType) {
      state.listed.splice(index, 1)
      return {
        ...state
      } 
    })

  const default$ = xs.of(function defaultReducer(_: ReducerInputType) {
      return { suggestions: [], cachedSuggestions: [],highlighted: null, selected: false, listed: [] }
  })  
 

  return xs.merge(  
    default$,
    suggestions$, 
    moveHighlightReducer$, 
    setHighlightReducer$,
    selectHighlightedReducer$,
    hideReducer$,
    removeItemReducer$ 
  )
}

function renderSelectedList({listed}: AutocompleteStateType) {
  const hasLength = listed.length
  const listStyle = hasLength ? selectedListStyle : noResultsListStyle
  return ul('.selected-list', {style: listStyle},
    hasLength ? listed
      .map((sel, index) =>
        li('.selected-item',
          {style: selectedItemStyle},
          [
            div([sel]),
            button({style: deleteXButtonStyle, attrs: {'data-index': index}}, [
              renderCloseIcon()
            ])
          ]
        )
      ) : [li({style: noResultsItemStyle}, [div({style: noResultsStyle}, ['No selected terms'])])]
  )
}

const getChildStyle = (highlighted: number | null, index: number) => ({
  ...autocompleteItemStyle, 
  backgroundColor: highlighted === index ? LIGHT_GRAY : null
})

function renderAutocompleteMenu({suggestions, highlighted}: AutocompleteStateType): VNode | null {
  return suggestions.length ? ul('.autocomplete-menu', {style: autocompleteMenuStyle},
    suggestions
      .map((suggestion, index) =>
        li('.autocomplete-item',
          {style: getChildStyle(highlighted, index), attrs: {'data-index': index}},
          suggestion
        )
      )
  ) : null
}

function renderComboBox(state: AutocompleteStateType): VNode {
  const {selected} = state

  return span('.combo-box', {style: comboBoxStyle}, [
    input('.autocompleteable', {
      style: autocompleteableStyle,
      attrs: {type: 'text', placeholder: 'Enter Wikipedia search term...'},
      hook: {
        update: (old: VNode, {elm}: VNode) => {
          if (selected === null) {
            (elm as HTMLInputElement).value = ''
          }
        }
      }
    }),
    renderAutocompleteMenu(state)
  ])
}

function view(state: AutocompleteStateType): VNode {
  return (
    div('.macro-container', {style: macroContainerStyle}, [
      div('.container', {style: containerStyle}, [
        section([
          renderComboBox(state)
        ]),
        renderSelectedList(state)
      ])
    ])
  )
}

const BASE_URL =
  'https://en.wikipedia.org/w/api.php?action=opensearch&format=json&search='

const networking = {
  processResponses(JSONP: JSONPSource): Stream<string[]> {
    return (JSONP as Stream<any>).filter(res$ => res$.request.indexOf(BASE_URL) === 0)
      .flatten<any[]>()
      .map(((res: any[]) => res[1] as string[]))
  }, 

  generateRequests(searchQuery$: Stream<string>): Stream<string> {
    return searchQuery$.map(q => BASE_URL + encodeURI(q))
  },
}

function preventedEvents(actions: ActionsType, state$: Stream<AutocompleteStateType>) {
  return state$
    .map(state =>
      actions.keepFocusOnInput$.map(event => {
        if (state.suggestions.length > 0 && state.highlighted !== null) {
          return event
        } else {
          return null
        }
      })
    )
    .flatten()
    .filter(ev => ev !== null) 
}

export default function app(sources: Sources): Sinks {
  const state$ = getStateStream(sources)
  const suggestionsFromResponse$ = networking.processResponses(sources.JSONP)
  const actions = intent(sources.DOM, sources.Time)
  const reducer$ = reducers(suggestionsFromResponse$, actions)
  const vtree$ = state$.map(view)
  const prevented$ = preventedEvents(actions, state$)
  const searchRequest$ = networking.generateRequests(actions.search$)
  return {
    DOM: vtree$,
    preventDefault: prevented$,
    JSONP: searchRequest$,
    state: reducer$
  }
}
