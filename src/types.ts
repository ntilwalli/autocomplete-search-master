import {Stream} from 'xstream'
import {VNode, MainDOMSource} from '@cycle/dom'
import {ResponseStream} from '@cycle/jsonp'
import {TimeSource} from '@cycle/time'
import {StateSource} from '@cycle/state'

export type AutocompleteStateType = {
    suggestions: string[]
    cachedSuggestions: string[]
    highlighted: number | null
    selected: boolean | null
    listed: string[]
}

export type ReducerInputType = AutocompleteStateType | undefined
export type AutocompleteReducerType = (state: AutocompleteStateType) => AutocompleteStateType

export type ActionsType = {
    search$: Stream<string>
    moveHighlight$: Stream<number>
    setHighlight$: Stream<number>
    keepFocusOnInput$: Stream<any>
    selectHighlighted$: Stream<any>
    wantsSuggestions$: Stream<boolean>
    quitAutocomplete$: Stream<any>
    removeItem$: Stream<any>
}

export type SuggestionsConfigType = {
    type: string
    results?: string[]
}
export type JSONPSource = Stream<ResponseStream>

export interface Sources {
    DOM: MainDOMSource
    JSONP: JSONPSource
    preventDefault: Stream<any>
    Time: TimeSource
    state: StateSource<AutocompleteStateType> 
}

export interface Sinks {
    DOM: Stream<VNode>
    JSONP: Stream<string>
    preventDefault: Stream<Event>,
    Time?: Stream<any>,
    state: Stream<AutocompleteReducerType>
}

export interface CycleMouseEvent extends MouseEvent {
    ownerTarget: HTMLElement
}

export {
    MainDOMSource,
    TimeSource
}
